const divAlert = document.querySelector('#divAlert');

document.addEventListener('DOMContentLoaded', function () { 
    const btnSubmit = document.querySelector('#btnSubmit');
    btnSubmit.addEventListener('click', validationForm);
    divAlert.style.visibility = "hidden";
});

function validationForm(event) {
   
    let valid = true;
    let msgAlert = "";

    // Selection des inputs du formulaire
    const civilite = document.querySelector('#civilite');
    const nom = document.querySelector('#nom');
    const prenom = document.querySelector('#prenom');
    const age = document.querySelector('#age');
    const email = document.querySelector('#email');
    const nomRue = document.querySelector('#nomRue');
    const numRue = document.querySelector('#numRue');
    const codePostal = document.querySelector('#codePostal');
    const ville = document.querySelector('#ville');
    const pays = document.querySelector('#pays');
    const modale = document.querySelector('#monModal');

    // Vérification champ 'civilité' : femme ou homme
    if (  civilite.value !== 'Femme' && civilite.value !=='Homme') {
        valid = false;
        msgAlert = msgAlert + "La civilité est incorrecte" + "<br>";
    }
    // Vérification champ 'nom' : entre 2 et 40 caractères
    if (   nom.value ==""    || nom.value.length <2  || nom.value.length>40) {
        valid = false;
        msgAlert = msgAlert + "Le nom doit etre renseigné et comporté entre 2 et 40 caractères " + "<br>";
    }
    // Vérification champ 'prénom' => UTILISATION DE L API VALIDITY
    if (   prenom.value ==""    || prenom.validity.tooShort  || nom.value.length>40) {
        valid = false;
        msgAlert = msgAlert + "Le prénom doit etre renseigné et comporté entre 2 et 40 caractères " + "<br>";
    }
    // Vérification champ 'date de naissance' : présence valeur
    if ( (datenaissance.value == "")  ) {
        valid = false;
        msgAlert = msgAlert + "L'age doit etre renseigné " + "<br>";
    }
    // Vérification champ 'date de naissance' : age > 18 ans
    if (  (calculAge(datenaissance.value) < 18)  ) {
        valid = false;
        msgAlert = msgAlert + "La personne doit etre majeure " + "<br>";
    }
    // Vérification champ 'code postal' : 5 caractères exactement
    if (   codePostal.value.length != 5  ) {
        valid = false;
        msgAlert = msgAlert + "Le code postal doit comporter exactement 5 caractères  " + "<br>";
    }
    // Vérification champ 'nom rue' : présence valeur
    if (   nomRue.value.length == 0  ) {
        valid = false;
        msgAlert = msgAlert + "Le nom de la rue doit etre renseigné" + "<br>";
    }
    // Vérification champ 'num rue' : valeur positive
    if (   numRue.value <= 0  ) {
        valid = false;
        msgAlert = msgAlert + "Le numéro de la rue etre un nombre positif " + "<br>";
    }
    // Vérification champ 'email' : valeur conforme
    if ( ! validateEmail(email.value)  ) {
        valid = false;
        msgAlert = msgAlert + "Adresse email incorrecte " + "<br>";
    }
    // Vérification champ 'ville' : présence valeur
    if (   ville.value.length == 0  ) {
        valid = false;
        msgAlert = msgAlert + "La ville doit etre renseigné" + "<br>";
    }
    // Vérification champ 'pays' : présence valeur
    if (   pays.value.length == 0  ) {
        valid = false;
        msgAlert = msgAlert + "Le pays doit etre renseigné" + "<br>";
    }

    // Formulaire invalide
    if (valid === false) {
        divAlert.style.visibility = "visible";
        divAlert.innerHTML = msgAlert;
        event.preventDefault();
    }

    // Formulaire valide
    if (valid === true) {
        divAlert.style.visibility = "hidden";
        msgAlert = "Le formulaire a été envoyé.";
        alert(msgAlert);
    }
}

// Fonction controle adresse email
function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

// Fonction calcul de l'age
function calculAge( dateNaissance){
    let td = new Date();    
    let an = dateNaissance.substr(0,4);
    let age = td.getFullYear() - an;
    return age;
}









